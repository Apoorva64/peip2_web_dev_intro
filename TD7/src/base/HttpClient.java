

import java.net.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Objects;
import javax.net.ssl.*;

import static java.lang.Thread.sleep;

/**
 * This class is able to build a request, send it, and give back the answer
 * This class uses the HTTP/1.1 protocol.
 */
public class HttpClient {

    private final String svrName;
    private final int svrPort;
    private String httpHeader = "";
    private String httpBody = "";
    private boolean isSecure = true;

    /**
     * Creates a new HttpClient that will connect to svrName through svrPort
     *
     * @param svrName The server name or IP address in String
     * @param svrPort The server port (int).
     **/
    public HttpClient(String svrName, int svrPort, boolean isSecure) {
        this.svrName = svrName;
        this.svrPort = svrPort;
        this.isSecure = isSecure;
    }

    /**
     * createReq() creates the first line of a new request, and append
     * the "Host" HTTP option to create a new
     * Ex. GET /index.php?var1=val1&var2=val2 HTTP/1.1
     * This first line must be added to the "httpHeader" class attribute
     * This method will add the CRLF characters at the end of the line!
     *
     * @param method The method (e.g. GET), in String
     * @param path   The URI
     **/
    public void createReq(String method, String path) {
        httpHeader += method + " " + path + " HTTP/1.1\r\n";
        httpHeader += "Host: " + svrName.replace("http://", "").replace("https://", "") + "\r\n";
    }

    /**
     * Add a line to the HTTP request header (i.e. a HTTP option field).
     * Ex. Content-Type: application/x-www-form-urlencoded
     * This method will add the CRLF characters at the end of the option.
     *
     * @param headerLine The line to add to the HTTP header. "headerLine" must not contain the CRLF characters.
     **/
    public void addHeaderLine(String headerLine) {
        httpHeader += headerLine + "\r\n";
    }

    /**
     * Add the bodyData string to the HTTP request body. addBodyData()
     * can be called multiple times. At every call, the new bodyData
     * is concatenated at the end of the "httpBody" class attribute.
     *
     * @param bodyData The data to add to the HTTP body. bodyData must not contain a "&".
     **/
    public void addBodyData(String bodyData) {
        if (!Objects.equals(bodyData, "")) {
            httpBody += "&" + bodyData;
        } else {
            httpBody += bodyData;
        }
    }

    /**
     * Print the full request to be sent. This is just for debugging purposes.
     **/
    private void showFullRequest() {
        System.out.println("=== Req to Send ===");
        System.out.println(httpHeader + "\r\n" + httpBody);
    }

    /**
     * Helper to read a line from an InputStream.
     **/
    private String myreadline(InputStream is) throws IOException {
        char c = 0;
        int len = 0;
        StringBuilder s = new StringBuilder();
        do {
            len = is.read();
            // did the remote peer close the connexion?
            if (len == -1) {
                if (s.length() == 0) return null;
                else return s.toString();
            }
            c = (char) len;
            // is it the end of the line?
            if (c == '\n') break;
            // CR is not a line end
            if (c == '\r') continue;
            s.append(c);
        } while (c != -1);
        return s.toString();
    }

    /**
     * Actually send the created request and closes the connection.
     * This method will add the "Content-Length" option if there is
     * a body for the request to send.
     * In all cases, sendRequest() erases the current request before
     * leaving this method.
     *
     * @return A String array. Index zero will contain the
     * HTTP header reply. Index 1 will contain the HTTP body reply
     * (if any)
     **/
    public String[] sendRequest() {
        String[] res = {"", ""};

        Socket s;
        if (isSecure)
            s = getSecureSocket();
        else
            s = getPlainSocket();

        // check if there is a body for our request
        if (!Objects.equals(httpBody, "")) {
            // if so, add the Content-Length parameter with the size
            // of the body in bytes.
            // To get the number of bytes in a String s, you can do
            int lenBytes = httpBody.getBytes(StandardCharsets.UTF_8).length;
            addHeaderLine("Content-Length: " + lenBytes);
        }

        try {
            // The socket is already open. We need however to create a
            // 1. PrintWriter object to send a message to the server
            assert s != null;
            PrintWriter sender = new PrintWriter(s.getOutputStream());
            sender.print(httpHeader + "\r\n" + httpBody);
            // 2. InputStream object to read the message sent by the server
            InputStream receiver = s.getInputStream();
            // after sending a message, flush() the PrintWriter object
            // ex. out.flush()
            sender.flush();

            BufferedReader reader = new BufferedReader(new InputStreamReader(receiver));
            boolean InBody = false;
            String line;
            // read every line from the server
            while ((line = reader.readLine()) != null) {
                if (Objects.equals(line, "\r\n") || Objects.equals(line, "\n") || Objects.equals(line, "")) {
                    InBody = true;
                    continue;
                }
                if (!InBody) {
                    res[0] += line + "\n";
                } else {
                    res[1] += line + "\n";
                }
            }


            // put the full header into res[0]
            // and the full body into res[1]
            // header and body are separated by an empty string
            // warning!! the body can have multiple empty lines!!
            // look at the slides from our course to complete this part
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
        httpHeader = "";
        httpBody = "";
        return res;
    }

    private Socket getSecureSocket() {
        try {
            SSLSocketFactory factory =
                    (SSLSocketFactory) SSLSocketFactory.getDefault();
            return factory.createSocket(svrName, svrPort);
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
        return null;
    }

    private Socket getPlainSocket() {
        try {
            return new Socket(svrName, svrPort);
        } catch (UnknownHostException ex) {
            System.out.println("Server not found: " + ex.getMessage());
        } catch (IOException ex) {
            System.out.println("I/O error: " + ex.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        // Demo to show how the HttpClient is expected to work
        String[] reply;
        // We're using HTTP
        HttpClient hc = new HttpClient("www.i3s.unice.fr", 80, false);
        hc.createReq("GET", "/");
        hc.addHeaderLine("Connection: close");
        hc.showFullRequest();
        reply = hc.sendRequest();
        System.out.print("Header:\n" + reply[0]);
        System.out.println("Body:\n" + reply[1]);

        // We're using HTTPS
        hc = new HttpClient("www.i3s.unice.fr", 443, true);
        hc.createReq("GET", "/~lopezpac/");
        hc.addHeaderLine("Connection: close");
        reply = hc.sendRequest();
        System.out.print("Header:\n" + reply[0]);
        System.out.println("Body:\n" + reply[1]);


        hc = new HttpClient("httpbin.org", 80, false);
        hc.createReq("GET", "/get?var1=val1&var2=val2");
        hc.addHeaderLine("Connection: close");
        reply = hc.sendRequest();
        System.out.print("Header:\n" + reply[0]);
        System.out.println("Body:\n" + reply[1]);
        hc.createReq("POST", "/post?var1=val1&var2=val2");
        hc.addHeaderLine("Connection: close");
        hc.addHeaderLine("Content-Type: application/x-www-form-urlencoded");
        hc.addBodyData("var3=val3");
        hc.addBodyData("var4=val4");
        reply = hc.sendRequest();
        System.out.print("Header:\n" + reply[0]);
        System.out.println("Body:\n" + reply[1]);

    }
}