package td.revisions;

public class Etudiant {
    final String nom;
    final String prenom;
    final String classe;
    final int rang;

    public Etudiant(String nom, String prenom, String classe, int rang) {
        this.nom = nom;
        this.prenom = prenom;
        this.classe = classe;
        this.rang = rang;
    }

    @Override
    public String toString() {
        return nom + ' ' + prenom + ", promo " + classe + ", rang " + rang;
    }
}
