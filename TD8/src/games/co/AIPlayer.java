package games.co;

public class AIPlayer extends Player {

    public AIPlayer(HttpClient restClient, String gameId) {
        this.restClient = restClient;
        this.gameId = gameId;
        this.playerName = "AI";
    }

    /**
     * This method will request the server to use the AI to move a piece
     */
    public int move() {
        restClient.createReq("POST", "/api/v1/chess/one/move/ai");
        restClient.addHeaderLine("Content-Type: application/x-www-form-urlencoded");
        restClient.addHeaderLine("Connection: close");
        restClient.addBodyData("game_id="+gameId);
        String[] response = restClient.sendRequest();
        //Once the reply has been obtained, we need to check if we
        //received a "200 OK"
        if (response[0].contains("200 OK")) {
            if (response[1].contains("error")) {
                //If it is a "200 OK" but the JSON object contains
                //"error", then return -1
                return -1;
            }
        } else {
            //If not, return à -1
            return -1;
        }
        // in all other cases, return 0 (success code)
        return 0;
    }

}